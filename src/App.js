import React, { useState } from "react";
import _ from "lodash";
import styled from "styled-components";
import { DndProvider, useDrag, useDrop } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { rhythm, column, gutter, DarkGray, maxAppWidth } from "./lib";
import { sofiLogo, reactLogo } from "./images";
const mockResponse = require("./__tests__/mockGiphyApiResponse.json");

const AppPageContainer = styled.section`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  max-width: ${maxAppWidth}px;
  margin-right: auto;
  margin-left: auto;

  // non-prod CSS guardrails
  ${() => {
    if (process.env.NODE_ENV !== "production") {
      /* Accessibility: All imgs must have an alt attribute,
       * see https://webaim.org/techniques/alttext/
       */
      return `
      img:not([alt]) {
        border: 5px dashed #c00 !important;
      }
    `;
    } else {
      return ``;
    }
  }};
`;

const AppHeader = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: ${DarkGray};
  max-height: ${rhythm(4)}px;

  .react-logo-animation {
    animation: App-logo-spin infinite 20s linear;
    height: ${rhythm(2)}px;
    pointer-events: none;
  }

  @keyframes App-logo-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;

const StyledGif = styled.div`
  margin: 5px;
  padding: 3px;
  border: 1px solid black;
  display: inline-block;
`;

const Page = styled.div`
  padding: ${rhythm(2)}px ${column + gutter}px ${rhythm(3)}px ${column - gutter}px;
  @media (max-width: 768px) {
    padding: ${rhythm(1)}px ${gutter}px;
  }
`;

const StyledGifList = styled.section`
  min-height: ${rhythm(20)}px;
`;

const StyledDropzone = styled.section`
  border: 1px solid ${DarkGray};
  min-height: ${rhythm(10)}px;
  margin-top: 10px;
`;

const DndTypes = {
  RESULT: "result",
};

const DraggableGifObject = (props) => {
  const [draggedOut, setDraggedOut] = useState(false);
  const [{ isDragging }, drag] = useDrag({
    // props.slug contains the image metadata.
    item: { elem: props.slug, type: DndTypes.RESULT },
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      if (item && dropResult) {
        setDraggedOut(true);
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  if (draggedOut) {
    return null;
  }

  return (
    <StyledGif>
      <div ref={drag} key={props.slug.id}>
        {<img src={props.slug.src} height={props.slug.height} width={props.slug.width} alt={props.slug.alt} />}
      </div>
    </StyledGif>
  );
};

const Dropzone = () => {
  const [images, setImages] = useState([]);
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: DndTypes.RESULT,
    drop: (item, monitor) => {
      // Image got dropped, add it to our list of droppped imaged.
      setImages([...images, item]);
      return { name: "Dropzone Area" };
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  return (
    <div ref={drop}>
      <StyledDropzone>
        {images.map((d) => {
          return (
            <StyledGif>
              <img src={d.elem.src} height={d.elem.height} width={d.elem.width} alt={d.elem.alt} />
            </StyledGif>
          );
        })}
      </StyledDropzone>
    </div>
  );
};

const App = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [searchData, setSearchData] = useState([]);
  let data = [];
  const handleSearchInput = async (e) => {
    let search = e.target.value;
    setSearchQuery(search);

    // call giphy to get gifs for the search query.
    let url = `https://api.giphy.com/v1/gifs/search?api_key=BYdIEho6oIUxOb9vhu5qa6EO70d0FY6p&q=${search}&limit=25&offset=0&rating=g&lang=en`;
    await fetch(url)
      .then((res) => res.json())
      .then((resp) => {
        data = _.get(resp, "data", []);
      });

    setSearchData(data);
  };

  // Returns the grid of gifs retrieved for the search query.
  const getStyledGifList = () => {
    return (
      <StyledGifList>
        {searchData.map((d) => {
          const data = d.images.original;
          const slug = {
            id: data.id,
            src: data.url,
            height: "100px",
            width: "100px",
            alt: "img",
          };
          return <DraggableGifObject slug={slug} />;
        })}
      </StyledGifList>
    );
  };

  return (
    <DndProvider backend={HTML5Backend}>
      <AppPageContainer>
        <AppHeader>
          <img src={sofiLogo} alt="SoFi logo" />
          <img src={reactLogo} className="react-logo-animation" alt="React logo" />
        </AppHeader>
        <Page>
          <h2 style={{ textAlign: "center" }}> Drag Into the Dropzone </h2>
          <div style={{ height: 30, display: "flex", justifyContent: "center" }}>
            <input
              type="text"
              placeholder="Search here"
              value={searchQuery}
              onChange={handleSearchInput}
              style={{ textAlign: "center" }}
            />
          </div>
          {getStyledGifList()}
          <Dropzone />
        </Page>
      </AppPageContainer>
    </DndProvider>
  );
};

export default App;
